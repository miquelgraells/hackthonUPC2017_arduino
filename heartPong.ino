/*
code base from https://github.com/WorldFamousElectronics/PulseSensor_Amped_Arduino
*/


/*  PulseSensor™ Starter Project   http://www.pulsesensor.com
 *   
This an Arduino project. It's Best Way to Get Started with your PulseSensor™ & Arduino. 
-------------------------------------------------------------
1) This shows a live human Heartbeat Pulse. 
2) Live visualization in Arduino's Cool "Serial Plotter".
3) Blink an LED on each Heartbeat.
4) This is the direct Pulse Sensor's Signal.  
5) A great first-step in troubleshooting your circuit and connections. 
6) "Human-readable" code that is newbie friendly." 
*/

//  Variables
int pulsePin = 0;        // Pulse Sensor PURPLE WIRE connected to ANALOG PIN 0

// Volatile Variables, used in the interrupt service routine!
volatile int BPM;                   // int that holds raw Analog in 0. updated every 2mS
volatile int Signal;                // holds the incoming raw data
volatile int IBI = 600;             // int that holds the time interval between beats! Must be seeded!
volatile boolean Pulse = false;     // "True" when User's live heartbeat is detected. "False" when not a "live beat".
volatile boolean QS = false;        // becomes true when Arduoino finds a beat.

// The SetUp Function:
void setup() {  
   Serial.begin(115200);         // Set's up Serial Communication at certain speed.    
   interruptSetup();
}

// The Main Loop Function
void loop() {

  Serial.println(BPM);

  if (QS == true){     // A Heartbeat Was Found
                       // BPM and IBI have been Determined
                       // Quantified Self "QS" true when arduino finds a heartbeat           
        Serial.println("Bom, bom");      
        QS = false;                      // reset the Quantified Self flag for next time
  }
  delay(20);                             //  take a break  
}

